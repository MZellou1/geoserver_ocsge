python -m venv geoserver_ocsge
source geoserver_ocsge/bin/activate

# https://geoserver-rest.readthedocs.io/en/latest/installation.html
sudo add-apt-repository ppa:ubuntugis/ppa
sudo apt update -y; sudo apt upgrade -y;
sudo apt install gdal-bin libgdal-dev
/home/dl/dev/marouane/geoserver_ocsge/geoserver_ocsge/bin/pip3 install GDAL=="`gdal-config --version`.*"
/home/dl/dev/marouane/geoserver_ocsge/geoserver_ocsge/bin/pip3 install geoserver-rest
/home/dl/dev/marouane/geoserver_ocsge/geoserver_ocsge/bin/python3 -m ipykernel install --user --name=geoserver_ocsge

# python3 -m pip install --upgrade GDAL ???
