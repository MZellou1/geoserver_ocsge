from geo.Geoserver import Geoserver
import os
import requests

if __name__ == "__main__":

    geo = Geoserver('http://localhost:8600/geoserver', username='admin', password='geoserver')

    orthos = '/mnt/remote/TERRIA/DATASETS_DAI/ORTHO'
    dem = '/mnt/remote/TERRIA/DATASETS_DAI/DEM'
    vect = '/mnt/remote/TERRIA/DATASETS_DAI/LABELS_VECT'
    style = '/mnt/remote/TERRIA/DATASETS_DAI/style_vect.sld'
    
    dep_dir = [f for f in os.listdir(orthos) if 'D' in f]

    rvb_file = []
    rvb_file_url = []

    ir_file = []
    ir_file_url = []

    dsm_file_url = []
    dsm_file = []

    dtm_file_url = []
    dtm_file = []

    for dep in dep_dir:
        zone_dir = [f for f in os.listdir(f'{orthos}/{dep}') if '.vrt' not in f]
        
        geo.create_workspace(workspace=dep)
        
        for zone in zone_dir:
            rvb_file_url += [f'{orthos}/{dep}/{zone}/{f}' for f in os.listdir(f'{orthos}/{dep}/{zone}') if '_RVB' in f]
            rvb_file += [f.replace('.tif', '') for f in os.listdir(f'{orthos}/{dep}/{zone}') if '_RVB' in f and '.vrt' not in f]
            
            ir_file_url += [f'{orthos}/{dep}/{zone}/{f}' for f in os.listdir(f'{orthos}/{dep}/{zone}') if '_IR' in f]
            ir_file += [f.replace('.tif', '') for f in os.listdir(f'{orthos}/{dep}/{zone}') if '_IR' in f]
            
            dsm_file_url += [f'{dem}/{dep}/{zone}/{f}' for f in os.listdir(f'{dem}/{dep}/{zone}') if '_DSM' in f]
            dsm_file += [f.replace('.tif', '') for f in os.listdir(f'{dem}/{dep}/{zone}') if '_DSM' in f and '.vrt' not in f]
            
            dtm_file_url += [f'{dem}/{dep}/{zone}/{f}' for f in os.listdir(f'{dem}/{dep}/{zone}') if '_DTM' in f]
            dtm_file += [f.replace('.tif', '') for f in os.listdir(f'{dem}/{dep}/{zone}') if '_DTM' in f]



req = requests.get(url='http://localhost:8600/geoserver/rest/workspaces/tests/layergroups/tests_group2', auth=('admin', 'geoserver'), headers={"content-type": "text/xml", "accept": "application/xml"})
requests.put(url='http://localhost:8600/geoserver/rest/workspaces/tests/layergroups/tests_group2', data=req.content.replace(b'SINGLE', b'CONTAINER'), auth=('admin', 'geoserver'), headers={"content-type": "text/xml", "accept": "application/xml"})

requests.post(url='http://localhost:8600/geoserver/rest/workspaces/tests/layergroups/', data=req.content.replace(b'SINGLE', b'CONTAINER').replace(b'tests_group2', b'tests_group3'), auth=('admin', 'geoserver'), headers={"content-type": "text/xml", "accept": "application/xml"})